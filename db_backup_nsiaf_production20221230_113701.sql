-- MariaDB dump 10.19  Distrib 10.5.18-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: nsiaf_production
-- ------------------------------------------------------
-- Server version	10.5.18-MariaDB-0+deb11u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `name` varchar(230) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `vida_util` int(11) NOT NULL DEFAULT 0,
  `depreciar` tinyint(1) NOT NULL DEFAULT 0,
  `actualizar` tinyint(1) NOT NULL DEFAULT 0,
  `status` varchar(255) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_tokens`
--

DROP TABLE IF EXISTS `api_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `api_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `token` text NOT NULL,
  `fecha_expiracion` date DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_tokens`
--

LOCK TABLES `api_tokens` WRITE;
/*!40000 ALTER TABLE `api_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_proceedings`
--

DROP TABLE IF EXISTS `asset_proceedings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_proceedings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proceeding_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_asset_proceedings_on_proceeding_id` (`proceeding_id`),
  KEY `index_asset_proceedings_on_asset_id` (`asset_id`),
  CONSTRAINT `fk_rails_574cdc3eb5` FOREIGN KEY (`proceeding_id`) REFERENCES `proceedings` (`id`),
  CONSTRAINT `fk_rails_dc06661115` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_proceedings`
--

LOCK TABLES `asset_proceedings` WRITE;
/*!40000 ALTER TABLE `asset_proceedings` DISABLE KEYS */;
/*!40000 ALTER TABLE `asset_proceedings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `auxiliary_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `observation` text DEFAULT NULL,
  `proceso` varchar(255) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `precio` decimal(10,2) NOT NULL DEFAULT 0.00,
  `detalle` varchar(255) DEFAULT NULL,
  `medidas` varchar(255) DEFAULT NULL,
  `material` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `marca` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  `serie` varchar(255) DEFAULT NULL,
  `code_old` varchar(255) DEFAULT NULL,
  `ingreso_id` int(11) DEFAULT NULL,
  `ubicacion_id` int(11) DEFAULT NULL,
  `baja_id` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_assets_on_auxiliary_id` (`auxiliary_id`),
  KEY `index_assets_on_user_id` (`user_id`),
  KEY `index_assets_on_account_id` (`account_id`),
  KEY `index_assets_on_ingreso_id` (`ingreso_id`),
  KEY `index_assets_on_ubicacion_id` (`ubicacion_id`),
  KEY `index_assets_on_baja_id` (`baja_id`),
  KEY `index_assets_on_asset_id` (`asset_id`),
  CONSTRAINT `fk_rails_17984c4e2f` FOREIGN KEY (`baja_id`) REFERENCES `bajas` (`id`),
  CONSTRAINT `fk_rails_590236dc2e` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_rails_61e74d4630` FOREIGN KEY (`ubicacion_id`) REFERENCES `ubicaciones` (`id`),
  CONSTRAINT `fk_rails_823c31e179` FOREIGN KEY (`ingreso_id`) REFERENCES `ingresos` (`id`),
  CONSTRAINT `fk_rails_fad3a5ab3c` FOREIGN KEY (`auxiliary_id`) REFERENCES `auxiliaries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets_seguros`
--

DROP TABLE IF EXISTS `assets_seguros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets_seguros` (
  `asset_id` int(11) NOT NULL,
  `seguro_id` int(11) NOT NULL,
  KEY `index_assets_seguros_on_asset_id_and_seguro_id` (`asset_id`,`seguro_id`),
  KEY `index_assets_seguros_on_seguro_id_and_asset_id` (`seguro_id`,`asset_id`),
  CONSTRAINT `fk_rails_0357afe86a` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`),
  CONSTRAINT `fk_rails_13cf7e4e44` FOREIGN KEY (`seguro_id`) REFERENCES `seguros` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets_seguros`
--

LOCK TABLES `assets_seguros` WRITE;
/*!40000 ALTER TABLE `assets_seguros` DISABLE KEYS */;
/*!40000 ALTER TABLE `assets_seguros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auxiliaries`
--

DROP TABLE IF EXISTS `auxiliaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auxiliaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `name` varchar(230) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_auxiliaries_on_account_id` (`account_id`),
  CONSTRAINT `fk_rails_96e19d2f9b` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auxiliaries`
--

LOCK TABLES `auxiliaries` WRITE;
/*!40000 ALTER TABLE `auxiliaries` DISABLE KEYS */;
/*!40000 ALTER TABLE `auxiliaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bajas`
--

DROP TABLE IF EXISTS `bajas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bajas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) DEFAULT NULL,
  `documento` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `motivo` varchar(255) DEFAULT NULL,
  `fecha_documento` date DEFAULT NULL,
  `documento_id` int(11) DEFAULT NULL,
  `documento_cite` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_bajas_on_user_id` (`user_id`),
  CONSTRAINT `fk_rails_1e8045912f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bajas`
--

LOCK TABLES `bajas` WRITE;
/*!40000 ALTER TABLE `bajas` DISABLE KEYS */;
/*!40000 ALTER TABLE `bajas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(230) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_buildings_on_code` (`code`),
  KEY `index_buildings_on_entity_id` (`entity_id`),
  CONSTRAINT `fk_rails_b2c90e921c` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cierre_gestiones`
--

DROP TABLE IF EXISTS `cierre_gestiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cierre_gestiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actualizacion_gestion` decimal(19,6) DEFAULT NULL,
  `depreciacion_gestion` decimal(19,6) DEFAULT NULL,
  `indice_ufv` decimal(6,5) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `gestion_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cierre_gestiones_on_asset_id` (`asset_id`),
  KEY `index_cierre_gestiones_on_gestion_id` (`gestion_id`),
  CONSTRAINT `fk_rails_8d61ed6000` FOREIGN KEY (`gestion_id`) REFERENCES `gestiones` (`id`),
  CONSTRAINT `fk_rails_c238942fca` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cierre_gestiones`
--

LOCK TABLES `cierre_gestiones` WRITE;
/*!40000 ALTER TABLE `cierre_gestiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `cierre_gestiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `name` varchar(230) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `building_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_departments_on_building_id` (`building_id`),
  CONSTRAINT `fk_rails_0381a2097b` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(230) DEFAULT NULL,
  `acronym` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `header` varchar(255) DEFAULT NULL,
  `footer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entities`
--

LOCK TABLES `entities` WRITE;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `entradas_salidas`
--

DROP TABLE IF EXISTS `entradas_salidas`;
/*!50001 DROP VIEW IF EXISTS `entradas_salidas`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `entradas_salidas` AS SELECT
 1 AS `id`,
  1 AS `subarticle_id`,
  1 AS `fecha`,
  1 AS `factura`,
  1 AS `nota_entrega`,
  1 AS `nro_pedido`,
  1 AS `detalle`,
  1 AS `cantidad`,
  1 AS `costo_unitario`,
  1 AS `modelo_id`,
  1 AS `tipo`,
  1 AS `created_at`,
  1 AS `cite_ems_plantillas` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `entry_subarticles`
--

DROP TABLE IF EXISTS `entry_subarticles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entry_subarticles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,4) DEFAULT NULL,
  `unit_cost` decimal(23,16) DEFAULT NULL,
  `total_cost` decimal(10,2) DEFAULT NULL,
  `invoice` varchar(255) DEFAULT '',
  `date` date DEFAULT NULL,
  `subarticle_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `note_entry_id` int(11) DEFAULT NULL,
  `invalidate` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_entry_subarticles_on_subarticle_id` (`subarticle_id`),
  KEY `fk_rails_ee711e9361` (`note_entry_id`),
  CONSTRAINT `fk_rails_589b865ebe` FOREIGN KEY (`subarticle_id`) REFERENCES `subarticles` (`id`),
  CONSTRAINT `fk_rails_ee711e9361` FOREIGN KEY (`note_entry_id`) REFERENCES `note_entries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entry_subarticles`
--

LOCK TABLES `entry_subarticles` WRITE;
/*!40000 ALTER TABLE `entry_subarticles` DISABLE KEYS */;
/*!40000 ALTER TABLE `entry_subarticles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gestiones`
--

DROP TABLE IF EXISTS `gestiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gestiones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anio` varchar(255) NOT NULL DEFAULT '',
  `cerrado` tinyint(1) NOT NULL DEFAULT 0,
  `fecha_cierre` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_gestiones_on_user_id` (`user_id`),
  CONSTRAINT `fk_rails_f5016c1841` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gestiones`
--

LOCK TABLES `gestiones` WRITE;
/*!40000 ALTER TABLE `gestiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `gestiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingresos`
--

DROP TABLE IF EXISTS `ingresos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) DEFAULT NULL,
  `nota_entrega_fecha` date DEFAULT NULL,
  `factura_numero` varchar(255) DEFAULT NULL,
  `factura_autorizacion` varchar(255) DEFAULT NULL,
  `factura_fecha` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `c31_numero` varchar(255) DEFAULT NULL,
  `baja_logica` tinyint(1) DEFAULT 0,
  `total` decimal(10,2) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nota_entrega_numero` varchar(255) DEFAULT NULL,
  `c31_fecha` date DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `incremento_alfabetico` varchar(255) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `documento_id` int(11) DEFAULT NULL,
  `documento_cite` varchar(255) DEFAULT NULL,
  `tipo_ingreso` varchar(255) DEFAULT NULL,
  `entidad_donante` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ingresos_on_supplier_id` (`supplier_id`),
  KEY `index_ingresos_on_user_id` (`user_id`),
  CONSTRAINT `fk_rails_19226c190b` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`),
  CONSTRAINT `fk_rails_ade1ffe7d1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingresos`
--

LOCK TABLES `ingresos` WRITE;
/*!40000 ALTER TABLE `ingresos` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingresos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materials`
--

DROP TABLE IF EXISTS `materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materials`
--

LOCK TABLES `materials` WRITE;
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note_entries`
--

DROP TABLE IF EXISTS `note_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_note_number` varchar(255) DEFAULT NULL,
  `delivery_note_date` date DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT '',
  `invoice_date` date DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `total` decimal(10,2) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `note_entry_date` date DEFAULT NULL,
  `invalidate` tinyint(1) DEFAULT 0,
  `message` varchar(255) DEFAULT NULL,
  `invoice_autorizacion` varchar(255) DEFAULT NULL,
  `c31` varchar(255) DEFAULT NULL,
  `nro_nota_ingreso` int(11) DEFAULT 0,
  `subtotal` decimal(10,2) DEFAULT 0.00,
  `descuento` decimal(10,2) DEFAULT 0.00,
  `incremento_alfabetico` varchar(255) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `c31_fecha` date DEFAULT NULL,
  `reingreso` tinyint(1) NOT NULL DEFAULT 0,
  `documento_id` int(11) DEFAULT NULL,
  `json_usuarios` text DEFAULT NULL,
  `documento_cite` varchar(255) DEFAULT NULL,
  `tipo_ingreso` varchar(255) DEFAULT NULL,
  `entidad_donante` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_note_entries_on_supplier_id` (`supplier_id`),
  KEY `fk_rails_ad8862a094` (`user_id`),
  CONSTRAINT `fk_rails_ad8862a094` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_rails_c41185cb6f` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_entries`
--

LOCK TABLES `note_entries` WRITE;
/*!40000 ALTER TABLE `note_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `note_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proceedings`
--

DROP TABLE IF EXISTS `proceedings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proceedings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `proceeding_type` varchar(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `baja_logica` tinyint(1) NOT NULL DEFAULT 0,
  `observaciones` varchar(255) DEFAULT 'Ninguno',
  `usuario_info` text DEFAULT NULL,
  `plantillas_info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_proceedings_on_user_id` (`user_id`),
  KEY `index_proceedings_on_admin_id` (`admin_id`),
  CONSTRAINT `fk_rails_3e0f21ce0b` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_rails_47fac28a6c` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proceedings`
--

LOCK TABLES `proceedings` WRITE;
/*!40000 ALTER TABLE `proceedings` DISABLE KEYS */;
/*!40000 ALTER TABLE `proceedings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT '0',
  `delivery_date` datetime DEFAULT NULL,
  `invalidate` tinyint(1) DEFAULT 0,
  `message` varchar(255) DEFAULT NULL,
  `nro_solicitud` int(11) DEFAULT 0,
  `incremento_alfabetico` varchar(255) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `documento_id` int(11) DEFAULT NULL,
  `json_usuarios` text DEFAULT NULL,
  `cite_sms` varchar(255) DEFAULT NULL,
  `cite_ems` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rails_8ead8b1e6b` (`user_id`),
  KEY `fk_rails_c38a214881` (`admin_id`),
  CONSTRAINT `fk_rails_8ead8b1e6b` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_rails_c38a214881` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20140130155734'),('20140130190858'),('20140131213135'),('20140131221812'),('20140203150058'),('20140204124844'),('20140204152552'),('20140204190653'),('20140204193616'),('20140204200956'),('20140206212200'),('20140206230226'),('20140210125017'),('20140210125836'),('20140210133133'),('20140210133723'),('20140210151839'),('20140221200122'),('20140221215141'),('20140221223439'),('20140225223301'),('20140225224514'),('20140324204659'),('20140401153237'),('20140403193643'),('20140403232128'),('20140404133200'),('20140407224022'),('20140624153627'),('20140624211316'),('20140624235111'),('20140625145042'),('20140630202211'),('20140701154149'),('20140702161118'),('20140704151611'),('20140714151216'),('20140714193648'),('20140724194838'),('20140728194547'),('20140728194802'),('20140902203711'),('20141030140546'),('20141114213130'),('20141117213257'),('20141120190622'),('20141120191307'),('20141120191600'),('20141120220431'),('20141120220915'),('20141121192208'),('20141205163100'),('20141208200222'),('20141211144015'),('20141215231216'),('20151013205922'),('20151015221615'),('20151016224030'),('20151016225947'),('20151020210508'),('20151022152510'),('20151022184846'),('20151029153727'),('20151029154153'),('20151228175105'),('20151229133044'),('20160111202036'),('20160111212746'),('20160122172818'),('20160127141532'),('20160209200136'),('20160209215614'),('20160229153322'),('20160303221209'),('20160310170230'),('20160311233203'),('20160314141650'),('20160315202926'),('20160418231153'),('20160426200237'),('20160426204748'),('20160513151525'),('20160713210616'),('20160713213423'),('20160714153321'),('20160714153827'),('20160715133659'),('20160801132730'),('20160804181050'),('20160804190719'),('20160805131947'),('20160805172834'),('20160805193007'),('20160809200836'),('20160811205607'),('20160811223411'),('20160811225219'),('20160812143959'),('20160817175059'),('20160818132357'),('20160906130717'),('20160923221500'),('20160929194914'),('20161002171713'),('20161003014617'),('20161014212727'),('20161014215536'),('20161019142524'),('20161031103748'),('20161031132408'),('20161107221322'),('20161116185150'),('20161121155851'),('20161123094938'),('20161208130735'),('20170203200501'),('20170203200857'),('20170203211000'),('20170203214454'),('20170203214629'),('20170203225558'),('20180222222802'),('20180223130011'),('20180223144500'),('20180308151738'),('20180326134822'),('20180919160555'),('20180919171212'),('20180919171510'),('20180919172406'),('20190206200942'),('20190206211353'),('20190717153045'),('20191119195042'),('20191120185000'),('20191125105200'),('20191126175600'),('20191206221833'),('20200128101819'),('20200203195641'),('20200317103000'),('20200602004757'),('20201117134437'),('20201204083000'),('20201204151600'),('20201209153000'),('20201221103000'),('20201221153000'),('20201223103000'),('20201223123231'),('20201231085624'),('20210112134500');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seguros`
--

DROP TABLE IF EXISTS `seguros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seguros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `numero_poliza` varchar(255) DEFAULT NULL,
  `numero_contrato` varchar(255) DEFAULT NULL,
  `factura_numero` varchar(255) DEFAULT NULL,
  `factura_autorizacion` varchar(255) DEFAULT NULL,
  `factura_fecha` date DEFAULT NULL,
  `factura_monto` double DEFAULT NULL,
  `fecha_inicio_vigencia` datetime DEFAULT NULL,
  `fecha_fin_vigencia` datetime DEFAULT NULL,
  `baja_logica` tinyint(1) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `seguro_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_seguros_on_seguro_id` (`seguro_id`),
  KEY `fk_rails_fbfcdaa8db` (`user_id`),
  KEY `fk_rails_7cf708c516` (`supplier_id`),
  CONSTRAINT `fk_rails_7cf708c516` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`),
  CONSTRAINT `fk_rails_8466f2fe6f` FOREIGN KEY (`seguro_id`) REFERENCES `seguros` (`id`),
  CONSTRAINT `fk_rails_fbfcdaa8db` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seguros`
--

LOCK TABLES `seguros` WRITE;
/*!40000 ALTER TABLE `seguros` DISABLE KEYS */;
/*!40000 ALTER TABLE `seguros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subarticle_requests`
--

DROP TABLE IF EXISTS `subarticle_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subarticle_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subarticle_id` int(11) DEFAULT NULL,
  `request_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `amount_delivered` decimal(10,2) DEFAULT NULL,
  `total_delivered` decimal(10,2) DEFAULT NULL,
  `invalidate` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_subarticle_requests_on_request_id` (`request_id`),
  KEY `index_subarticle_requests_on_subarticle_id` (`subarticle_id`),
  CONSTRAINT `fk_rails_67ff6e617c` FOREIGN KEY (`subarticle_id`) REFERENCES `subarticles` (`id`),
  CONSTRAINT `fk_rails_d89d70fde4` FOREIGN KEY (`request_id`) REFERENCES `requests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subarticle_requests`
--

LOCK TABLES `subarticle_requests` WRITE;
/*!40000 ALTER TABLE `subarticle_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `subarticle_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subarticles`
--

DROP TABLE IF EXISTS `subarticles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subarticles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `minimum` int(11) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `code_old` varchar(255) DEFAULT NULL,
  `incremento` int(11) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_subarticles_on_article_id` (`article_id`),
  KEY `index_subarticles_on_material_id` (`material_id`),
  CONSTRAINT `fk_rails_6fe93e487e` FOREIGN KEY (`material_id`) REFERENCES `materials` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subarticles`
--

LOCK TABLES `subarticles` WRITE;
/*!40000 ALTER TABLE `subarticles` DISABLE KEYS */;
/*!40000 ALTER TABLE `subarticles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `nit` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `contacto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ubicaciones`
--

DROP TABLE IF EXISTS `ubicaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ubicaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abreviacion` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ubicaciones`
--

LOCK TABLES `ubicaciones` WRITE;
/*!40000 ALTER TABLE `ubicaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `ubicaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ufvs`
--

DROP TABLE IF EXISTS `ufvs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ufvs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `valor` decimal(7,5) NOT NULL DEFAULT 0.00000,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ufvs`
--

LOCK TABLES `ufvs` WRITE;
/*!40000 ALTER TABLE `ufvs` DISABLE KEYS */;
/*!40000 ALTER TABLE `ufvs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT 0,
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `username` varchar(230) NOT NULL DEFAULT '',
  `code` int(11) DEFAULT NULL,
  `name` varchar(230) DEFAULT NULL,
  `title` varchar(230) DEFAULT NULL,
  `ci` varchar(255) DEFAULT NULL,
  `phone` varchar(230) DEFAULT NULL,
  `mobile` varchar(230) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `password_change` tinyint(1) NOT NULL DEFAULT 0,
  `assets_count` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`),
  KEY `index_users_on_email` (`email`),
  KEY `index_users_on_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@dominio.gob.bo','$2a$10$tCjG8WXqf1gpV0wQULBfAe7J0cQzrPWG7gRSMf8SAkpuBwmC6GhT6',NULL,NULL,NULL,3,'2022-12-28 20:49:16','2022-12-28 20:48:14','10.10.4.239','10.10.4.208','2022-12-28 18:40:25','2022-12-28 20:49:16','admin',NULL,'Administrador',NULL,NULL,NULL,NULL,'1',NULL,'super_admin',0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(255) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `event` varchar(255) NOT NULL,
  `whodunnit` varchar(255) DEFAULT NULL,
  `object` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `item_spanish` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_versions_on_item_type_and_item_id` (`item_type`,`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES (1,'User',1,'create',NULL,NULL,'2022-12-28 18:40:25',1,NULL),(2,'User',1,'Iniciar Sesión','1',NULL,'2022-12-28 20:47:11',1,'Funcionario'),(3,'User',1,'Cerrar Sesión','1',NULL,'2022-12-28 20:47:52',1,'Funcionario'),(4,'User',1,'Iniciar Sesión','1',NULL,'2022-12-28 20:48:14',1,'Funcionario'),(5,'User',1,'Cerrar Sesión','1',NULL,'2022-12-28 20:48:27',1,'Funcionario'),(6,'User',1,'Iniciar Sesión','1',NULL,'2022-12-28 20:49:16',1,'Funcionario'),(7,'User',1,'Cerrar Sesión','1',NULL,'2022-12-28 20:49:22',1,'Funcionario');
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'nsiaf_production'
--

--
-- Dumping routines for database 'nsiaf_production'
--
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP FUNCTION IF EXISTS `levenshtein` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `levenshtein`(s1 VARCHAR(255), s2 VARCHAR(255) ) RETURNS int(11)
    DETERMINISTIC
BEGIN
            DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
            DECLARE s1_char CHAR;
            -- max strlen=255
            DECLARE cv0, cv1 VARBINARY(256);

            SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0;

            IF s1 = s2 THEN
                RETURN 0;
            ELSEIF s1_len = 0 THEN
                RETURN s2_len;
            ELSEIF s2_len = 0 THEN
                RETURN s1_len;
            ELSE
                WHILE j <= s2_len DO
                    SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1;
                END WHILE;
                WHILE i <= s1_len DO
                    SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1;
                    WHILE j <= s2_len DO
                        SET c = c + 1;
                        IF s1_char = SUBSTRING(s2, j, 1) THEN
                            SET cost = 0; ELSE SET cost = 1;
                        END IF;
                        SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost;
                        IF c > c_temp THEN SET c = c_temp; END IF;
                        SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1;
                        IF c > c_temp THEN
                            SET c = c_temp;
                        END IF;
                        SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1;
                    END WHILE;
                    SET cv1 = cv0, i = i + 1;
                END WHILE;
            END IF;
            RETURN c;
        END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP FUNCTION IF EXISTS `saldo_final_v1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `saldo_final_v1`(subarticle_id INT, fecha_fin DATETIME) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
      DECLARE total DECIMAL(10, 2);
      DECLARE cantidad DECIMAL(10, 2);
      SELECT IFNULL(sum(stock), 0), IFNULL(SUM(saldo), 0) into cantidad, total
        FROM (SELECT @cantidad := IF(-stock < amount, -stock, amount) AS stock,
                     @cantidad * unit_cost AS saldo
                FROM (SELECT t1.note_entry_id,
                             t1.subarticle_id,
                             t1.entry_subarticle_id,
                             t1.entry_date,
                             t1.unit_cost,
                             t1.amount,
                             @salida := @salida - t1.amount AS stock
                        FROM (SELECT es.subarticle_id as subarticle_id,
                                     ne.id as note_entry_id,
                                     es.id as entry_subarticle_id,
                                     IFNULL(ne.note_entry_date, es.date) as entry_date,
                                     es.unit_cost as unit_cost,
                                     es.amount as amount
                                FROM (SELECT @salida := total_salidas(subarticle_id, fecha_fin)) salida, note_entries ne RIGHT JOIN entry_subarticles es on es.note_entry_id = ne.id
                                       WHERE (ne.invalidate = 0 OR (ne.invalidate is null and ne.id is null))
                                         AND es.subarticle_id = subarticle_id
                                         AND es.invalidate = 0
                                    ORDER BY entry_date, note_entry_id, entry_subarticle_id) t1
                       WHERE t1.entry_date <= fecha_fin) t2
               WHERE stock < 0) t3;
      return CONCAT(cantidad, '|', total);
    END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_ingresos_v1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `total_ingresos_v1`(subarticle_id INT, fecha_inicio DATETIME, fecha_fin DATETIME) RETURNS varchar(255) CHARSET utf8 COLLATE utf8_general_ci
BEGIN
        DECLARE total DECIMAL(10, 2);
        DECLARE cantidad DECIMAL(10, 2);
        select IFNULL(sum(t2.total), 0), IFNULL(sum(t2.amount), 0) into total, cantidad
          from (select t1.subarticle_id,
                       t1.note_entry_id,
                       t1.entry_subarticle_id,
                       t1.entry_date,
                       t1.unit_cost,
                       t1.amount,
                       (t1.unit_cost * t1.amount) as total
                  from (SELECT es.subarticle_id as subarticle_id,
                               ne.id as note_entry_id,
                               es.id as entry_subarticle_id,
                               IFNULL(ne.note_entry_date, es.date) as entry_date,
                               es.unit_cost as unit_cost,
                               es.amount as amount
                          FROM note_entries ne RIGHT JOIN entry_subarticles es on es.note_entry_id = ne.id
                         WHERE (ne.invalidate = 0 OR (ne.invalidate is null and ne.id is null))
                           AND es.subarticle_id = subarticle_id
                           AND es.invalidate = 0
                      ORDER BY entry_date, note_entry_id, entry_subarticle_id) t1 
                 where t1.entry_date <= fecha_fin
                   and t1.entry_date > fecha_inicio) t2;
        return CONCAT(cantidad, '|', total);
      END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_saldo_bs` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `total_saldo_bs`(subarticle_id INT, fecha DATETIME) RETURNS decimal(10,2)
BEGIN
        DECLARE total DECIMAL(10, 2) DEFAULT 0;
        SELECT SUM(saldo) INTO total
        FROM (
          SELECT id,
            subarticle_id,
            fecha,
            amount AS ingreso,
            @cantidad := IF(-stock < amount, -stock, amount) AS stock,
            unit_cost AS precio,
            @cantidad * unit_cost AS saldo
          FROM (SELECT t1.id,
                       t1.subarticle_id,
                       t1.note_entry_date,
                       t1.unit_cost,
                       t1.amount,
                       @salida := @salida - t1.amount AS stock
                  FROM (SELECT ne.id as id,
                               es.subarticle_id as subarticle_id,
                               ne.note_entry_date as note_entry_date,
                               es.unit_cost as unit_cost,
                               es.amount as amount
                          FROM (SELECT @salida := total_salidas(subarticle_id, fecha)) salida, note_entries ne
                                       RIGHT JOIN entry_subarticles es on es.note_entry_id = ne.id
                                 WHERE (ne.invalidate = 0 OR (ne.invalidate is null and ne.id is null))
                                   AND es.subarticle_id = subarticle_id
                                   AND (ne.note_entry_date <= fecha OR (ne.note_entry_date is null and ne.id is null))
                              ORDER BY ne.note_entry_date) t1) t2
          WHERE stock < 0) t3;
        RETURN COALESCE(total, 0);
      END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP FUNCTION IF EXISTS `total_salidas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `total_salidas`(subarticle_id INT, fecha DATETIME) RETURNS decimal(10,2)
BEGIN
        DECLARE total DECIMAL(10, 2) DEFAULT 0;
        SELECT SUM(sr.total_delivered) INTO total
        FROM requests r
          INNER JOIN subarticle_requests sr ON sr.request_id = r.id
        WHERE sr.subarticle_id = subarticle_id
          AND r.invalidate = 0
          AND r.status = 'delivered'
          AND r.delivery_date <= fecha;
        RETURN COALESCE(total, 0);
      END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `genera_kardex_v2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `genera_kardex_v2`(identificador INT, fecha_inicio VARCHAR(255), fecha_fin VARCHAR(255))
BEGIN
        DECLARE row_id INT;
        DECLARE row_fecha DATE;
        DECLARE row_tipo VARCHAR(255);
        DECLARE row_detalle VARCHAR(255);
        DECLARE row_cantidad DECIMAL(10,2);
        DECLARE row_costo_unitario DECIMAL(10,2);
        DECLARE row_factura VARCHAR(255);
        DECLARE row_nro_pedido VARCHAR(255);        
        DECLARE row_cite_ems_plantillas VARCHAR(255);
        DECLARE tipo_entrada VARCHAR(10) DEFAULT 'entrada';
        DECLARE entrada_id INT;
        DECLARE entrada_cantidad DECIMAL(10,2);
        DECLARE entrada_costo_unitario DECIMAL(10,2);
        DECLARE indice INT;
        DECLARE saldo_compensar DECIMAL(10,2);
        DECLARE random_name VARCHAR(255) default ROUND(RAND() * 1000000);
        DECLARE cantidad_entradas DECIMAL(10,2);
        DECLARE saldo_salidas DECIMAL(10,2);
        DECLARE n INT DEFAULT 0;
        DECLARE i INT DEFAULT 0;

        EXECUTE IMMEDIATE concat('CREATE OR REPLACE TEMPORARY TABLE kardex_', random_name, '_temp (nro INT(11), id INT(11), factura VARCHAR(255), nro_pedido VARCHAR(255), cite_ems_plantillas VARCHAR(255), fecha DATE, tipo VARCHAR(255), detalle VARCHAR(255), cantidad DECIMAL(10,2), costo_unitario DECIMAL(10,2));');
        EXECUTE IMMEDIATE concat('CREATE OR REPLACE TEMPORARY TABLE entradas_salidas_saldos_', random_name,'_temp (id INT(11), factura VARCHAR(255), nro_pedido VARCHAR(255), cite_ems_plantillas VARCHAR(255), fecha DATE, tipo VARCHAR(255), detalle VARCHAR(255), cantidad INT(11), costo_unitario DECIMAL(10,2));');
        CALL obtiene_saldos_iniciales_v2(identificador, fecha_inicio, random_name);
        EXECUTE IMMEDIATE concat('CREATE OR REPLACE TEMPORARY TABLE entradas_salidas_', random_name, '_temp
                                    SELECT id,
                                           factura,
                                           nro_pedido,
                                           cite_ems_plantillas,
                                           fecha,
                                           tipo,
                                           detalle,
                                           cantidad,
                                           costo_unitario
                                      FROM entradas_salidas_saldos_', random_name, '_temp 
                                 UNION ALL 
                                    SELECT es.id,
                                           es.factura,
                                           es.nro_pedido,
                                           es.cite_ems_plantillas,
                                           es.fecha,
                                           es.tipo,
                                           es.detalle,
                                           es.cantidad,
                                           es.costo_unitario
                                      FROM entradas_salidas es
                                     WHERE subarticle_id = ? 
                                       AND fecha BETWEEN cast(? as datetime) AND cast(? as datetime)
                                  ORDER BY fecha ASC, tipo ASC, id ASC;') USING identificador, fecha_inicio, fecha_fin; 

        EXECUTE IMMEDIATE concat('CREATE OR REPLACE TEMPORARY TABLE entradas_', random_name,'_temp 
                                    SELECT *
                                      FROM entradas_saldos_', random_name,'_temp 
                                 UNION ALL 
                                    SELECT id, fecha, tipo, cantidad, costo_unitario
                                      FROM entradas_salidas es
                                     WHERE subarticle_id = ? 
                                       AND tipo = ?
                                       AND cantidad > 0
                                       AND fecha BETWEEN cast(? as datetime) AND cast(? as datetime)
                                  ORDER BY fecha ASC, tipo ASC, id ASC;') USING identificador,'entrada', fecha_inicio, fecha_fin;
      
        EXECUTE IMMEDIATE concat('SELECT COUNT(*) FROM entradas_salidas_', random_name, '_temp INTO @n;');
        EXECUTE IMMEDIATE concat('SELECT COUNT(*) FROM kardex_', random_name, '_temp INTO @indice;');
        SET i=0;
        WHILE i < @n DO
          EXECUTE IMMEDIATE concat('SELECT id, factura, nro_pedido, cite_ems_plantillas, fecha, tipo, detalle, cantidad, costo_unitario INTO @row_id, @row_factura, @row_nro_pedido, @row_cite_ems_plantillas, @row_fecha, @row_tipo, @row_detalle, @row_cantidad, @row_costo_unitario FROM entradas_salidas_', random_name, '_temp LIMIT ?,1;') USING i;
      
          IF @row_tipo = 'entrada' THEN
            SET @indice = @indice + 1;
            EXECUTE IMMEDIATE concat('INSERT INTO kardex_', random_name, '_temp (nro, id, factura, nro_pedido, cite_ems_plantillas, fecha, tipo, detalle, cantidad, costo_unitario) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);') USING @indice, @row_id, @row_factura, @row_nro_pedido, @row_cite_ems_plantillas, @row_fecha, @row_tipo, @row_detalle, @row_cantidad, @row_costo_unitario;
          ELSE
            SET @row_cantidad = -1 * @row_cantidad;
            label1: REPEAT
              EXECUTE IMMEDIATE concat('DELETE FROM entradas_', random_name,'_temp WHERE cantidad = 0;');
              EXECUTE IMMEDIATE concat('SELECT id,
                                               costo_unitario,
                                               cantidad
                                          INTO @entrada_id,
                                               @entrada_costo_unitario,
                                               @entrada_cantidad
                                          FROM entradas_', random_name,'_temp LIMIT 1;'); 
              SET @indice = @indice + 1;
              IF (@entrada_cantidad - @row_cantidad) >= 0 THEN
                SET saldo_compensar = 0;
                EXECUTE IMMEDIATE concat('UPDATE entradas_', random_name,'_temp set cantidad = (',@entrada_cantidad - @row_cantidad,') where id = ',@entrada_id,';');
                EXECUTE IMMEDIATE concat('INSERT INTO kardex_', random_name, '_temp(nro, id, factura, nro_pedido, cite_ems_plantillas, fecha, tipo, detalle, cantidad, costo_unitario) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);') USING @indice, @row_id, @row_factura, @row_nro_pedido, @row_cite_ems_plantillas, @row_fecha, @row_tipo, @row_detalle, -1*@row_cantidad, @entrada_costo_unitario;              
              ELSE
                SET saldo_compensar = @row_cantidad - @entrada_cantidad;
                EXECUTE IMMEDIATE concat('UPDATE entradas_', random_name,'_temp set cantidad = 0 where id = ',@entrada_id,';');
                EXECUTE IMMEDIATE concat('INSERT INTO kardex_', random_name, '_temp(nro, id, factura, nro_pedido, cite_ems_plantillas, fecha, tipo, detalle, cantidad, costo_unitario) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);') USING @indice, @row_id, @row_factura, @row_nro_pedido, @row_cite_ems_plantillas, @row_fecha, @row_tipo, @row_detalle, -1*@entrada_cantidad, @entrada_costo_unitario;
                SET @row_cantidad = saldo_compensar;
              END IF;
            UNTIL saldo_compensar = 0
            END REPEAT label1;
          END IF;
          SET i = i + 1;
        END WHILE;

        EXECUTE IMMEDIATE concat('SELECT nro,
                                         id,
                                         factura,
                                         nro_pedido, 
                                         cite_ems_plantillas,
                                         fecha,
                                         tipo,
                                         detalle,
                                         costo_unitario,
                                         IF(cantidad >= 0, cantidad, 0)AS cantidad_ingreso,
                                         IF(cantidad < 0, cantidad*-1,0)AS cantidad_egreso,
                                         SUM(cantidad) OVER(ORDER BY NRO ASC)AS cantidad_saldo,
                                         IF(cantidad >= 0, cantidad*costo_unitario, 0) AS importe_ingreso,
                                         IF(cantidad < 0, cantidad*-1*costo_unitario,0) AS importe_egreso,
                                         SUM(cantidad*costo_unitario) OVER(ORDER BY NRO ASC)AS importe_saldo
                                    FROM kardex_', random_name, '_temp ORDER BY nro;');
        
        EXECUTE IMMEDIATE concat('DROP TEMPORARY TABLE ', 'kardex_', random_name, '_temp;'); 
        EXECUTE IMMEDIATE concat('DROP TEMPORARY TABLE ', 'entradas_', random_name, '_temp;');
        EXECUTE IMMEDIATE concat('DROP TEMPORARY TABLE ', 'entradas_saldos_', random_name, '_temp;');
        EXECUTE IMMEDIATE concat('DROP TEMPORARY TABLE ', 'entradas_salidas_', random_name, '_temp;');
        EXECUTE IMMEDIATE concat('DROP TEMPORARY TABLE ', 'entradas_salidas_saldos_', random_name, '_temp;');
      END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES' */ ;
/*!50003 DROP PROCEDURE IF EXISTS `obtiene_saldos_iniciales_v2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtiene_saldos_iniciales_v2`(IN identificador INT, IN fecha_inicio VARCHAR(255), IN random_name VARCHAR(255))
BEGIN
        DECLARE entrada_id INT;
        DECLARE entrada_cantidad DECIMAL(10,2);
        DECLARE cantidad_entradas DECIMAL(10,2);
        DECLARE saldo_salidas DECIMAL(10,2);

        # Obtiene las entradas antes de la fecha de inicio
        EXECUTE IMMEDIATE concat('CREATE OR REPLACE TEMPORARY TABLE entradas_saldos_', random_name,'_temp 
                                    SELECT id, fecha, tipo, cantidad, costo_unitario
                                      FROM entradas_salidas es
                                     WHERE subarticle_id = ?
                                       AND tipo = ?
                                       AND cantidad != 0
                                       AND fecha < cast(?  as datetime)
                                  ORDER BY fecha ASC, tipo ASC, id ASC;') USING identificador, 'entrada', fecha_inicio;
      
        EXECUTE IMMEDIATE concat('SELECT COUNT(*) INTO @cantidad_entradas FROM entradas_saldos_', random_name,'_temp;');
        SELECT -1 * sum(cantidad)
          INTO saldo_salidas
          FROM entradas_salidas es
         WHERE subarticle_id = identificador
           AND tipo = 'salida'
           AND fecha < cast(fecha_inicio  as datetime)
         ORDER BY fecha ASC, tipo ASC, id ASC;
        WHILE @cantidad_entradas > 0 AND saldo_salidas > 0 DO
          EXECUTE IMMEDIATE concat('SELECT id,
                                           cantidad
                                      INTO @entrada_id,
                                           @entrada_cantidad
                                      FROM entradas_saldos_', random_name,'_temp LIMIT 1;');
          IF (saldo_salidas - @entrada_cantidad) >= 0  THEN
            EXECUTE IMMEDIATE concat('UPDATE entradas_saldos_', random_name,'_temp set cantidad = 0 where id = ?;') USING @entrada_id;
            SET saldo_salidas = saldo_salidas - @entrada_cantidad;
          ELSE
            EXECUTE IMMEDIATE concat('UPDATE entradas_saldos_', random_name,'_temp set cantidad = (? - ?) where id = ?;') USING @entrada_cantidad, saldo_salidas, @entrada_id;
            SET saldo_salidas =  0;
          END IF;
          EXECUTE IMMEDIATE concat('DELETE FROM entradas_saldos_', random_name,'_temp WHERE cantidad = 0;');
          SET @cantidad_entradas = @cantidad_entradas - 1;
        END WHILE;
        IF saldo_salidas > 0 THEN
          # Adicionarlo como salida en el cursor de entradas y salidas
          EXECUTE IMMEDIATE concat('INSERT INTO entradas_salidas_saldos_', random_name,'_temp (fecha, tipo, detalle, cantidad) VALUES (?, ?, ?, ?);') USING cast(fecha_inicio as datetime), 'salida', 'SALDO INICIAL', -1 * saldo_salidas;
        END IF;
        EXECUTE IMMEDIATE concat('SELECT COUNT(*) INTO @cantidad_entradas FROM entradas_saldos_', random_name,'_temp;');
        IF @cantidad_entradas > 0 THEN
          EXECUTE IMMEDIATE concat('CREATE OR REPLACE TEMPORARY TABLE kardex_', random_name, '_temp (nro INT(11), id INT(11), factura VARCHAR(255), nro_pedido VARCHAR(255), cite_ems_plantillas VARCHAR(255), fecha DATE, tipo VARCHAR(255), detalle VARCHAR(255), cantidad DECIMAL(10,2), costo_unitario DECIMAL(10,2));');
          # Adicionarlo como salida en el cursor de entradas y salidas
          EXECUTE IMMEDIATE concat('INSERT INTO kardex_', random_name, '_temp (nro, id, fecha, tipo, detalle, cantidad, costo_unitario)
                                    SELECT ROW_NUMBER() OVER W AS nro,
                                           id,
                                           ?,
                                           tipo,
                                           ?,
                                           cantidad,
                                           costo_unitario 
                                      FROM entradas_saldos_', random_name, '_temp
                               WINDOW W AS (ORDER BY fecha ASC, tipo ASC, id ASC);') USING CAST(fecha_inicio as DATE),'SALDO INICIAL';
        END IF;
      END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `entradas_salidas`
--

/*!50001 DROP VIEW IF EXISTS `entradas_salidas`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `entradas_salidas` AS select `es`.`id` AS `id`,`es`.`subarticle_id` AS `subarticle_id`,`r`.`delivery_date` AS `fecha`,'' AS `factura`,cast(NULL as date) AS `nota_entrega`,`r`.`nro_solicitud` AS `nro_pedido`,concat(`u`.`name`,' - ',`u`.`title`) AS `detalle`,-`es`.`total_delivered` AS `cantidad`,0 AS `costo_unitario`,`es`.`request_id` AS `modelo_id`,'salida' AS `tipo`,`r`.`created_at` AS `created_at`,`r`.`cite_ems` AS `cite_ems_plantillas` from ((`requests` `r` join `subarticle_requests` `es` on(`r`.`id` = `es`.`request_id`)) join `users` `u` on(`r`.`user_id` = `u`.`id`)) where `r`.`invalidate` = 0 and `es`.`invalidate` = 0 and `r`.`status` = 'delivered' union select `es`.`id` AS `id`,`es`.`subarticle_id` AS `subarticle_id`,ifnull(`ne`.`note_entry_date`,`es`.`date`) AS `fecha`,`ne`.`invoice_number` AS `factura`,`ne`.`delivery_note_date` AS `nota_entrega`,'' AS `nro_pedido`,if(`ne`.`supplier_id`,(select `s`.`name` from `suppliers` `s` where `s`.`id` = `ne`.`supplier_id`),if(`ne`.`reingreso` = 1,'REINGRESO',if(`ne`.`tipo_ingreso` = 'donacion_transferencia','DONACION O TRANSFERENCIA','SALDO INICIAL'))) AS `detalle`,`es`.`amount` AS `cantidad`,`es`.`unit_cost` AS `costo_unitario`,`es`.`note_entry_id` AS `modelo_id`,'entrada' AS `tipo`,`es`.`created_at` AS `created_at`,`ne`.`documento_cite` AS `cite_ems_plantillas` from (`entry_subarticles` `es` left join `note_entries` `ne` on(`es`.`note_entry_id` = `ne`.`id`)) where `es`.`invalidate` = 0 order by `subarticle_id`,`fecha`,`id`,`cantidad` desc,`created_at` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-30 11:37:06
